<?php

/**
 * Plugin Name: Custom Slider Block
 * Description: Zadanie testowe Gogo Media.
 * Version: 1.1.0
 * Author: Przemek Misztal
 *
 */

defined( 'ABSPATH' ) || exit;


function custom_slider_block_register() {

	// automatically load dependencies and version
	$asset_file = include( plugin_dir_path( __FILE__ ) . 'index.asset.php');

	wp_register_script(
		'custom_slider',
		plugins_url( 'index.js', __FILE__ ),
		$asset_file['dependencies'],
		$asset_file['version']
	);

	wp_register_style(
		'custom_slider',
		plugins_url( 'style.css', __FILE__ ),
		array( ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);

	register_block_type( 'custom-slider/slider-block', array(
		'style' => 'custom_slider',
		'editor_script' => 'custom_slider',
	) );


}
add_action( 'init', 'custom_slider_block_register' );
