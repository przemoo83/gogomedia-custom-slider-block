import { RichText, MediaUpload } from '@wordpress/block-editor';
import { Button } from '@wordpress/components';

const Edit = (props)=>{

    return (
        <div className={props.className}>
            <div className="edit-heading edit-item">
                <h4 className="edit-heading">Heading</h4>
                <RichText
					tagName="h4"
					placeholder={'Heading...'}
					value={props.title}
					onChange={props.onChangeTitle}
				/>
            </div>
            <div className="edit-paragraph edit-item">
                <h4 className="edit-heading">Paragraph</h4>
                <RichText
					tagName="p"
					placeholder={'Paragraph...'}
					value={props.paragraph}
					onChange={props.onChangeParagraph}
				/>
            </div>
            <div className="edit-icon edit-item">
                <h4 className="edit-heading">Icon</h4>
                <MediaUpload
						onSelect={props.onSelectImage}
						allowedTypes="image"
						value={props.mediaID}
						render={({ open }) => (
							<Button
								className={
									props.mediaID
										? 'image-button'
										: 'button button-large'
								}
								onClick={open}
							>
								{ !props.mediaID ? ('Upload Image') : (
									<img
										src={props.mediaURL}
										alt={'UploadImage'}
									/>
								)}
							</Button>
						)}
					/>
            </div>
        </div>
    )
}
export default Edit;