import { RichText, MediaUpload } from '@wordpress/block-editor';
const Save = (props) => {

    return (
        <div class="slider__block glide__slide">
            <RichText.Content className="slider__block__heading" tagName="h4" value={props.title} />
            <RichText.Content
                tagName="p"
                className="slider__block__paragraph"
                value={props.paragraph}
            />
            <div className="slider__block__icon">
                {props.mediaURL && (
                    <img
                        className="icon-image"
                        src={props.mediaURL}
                        alt={props.mediaAlt}
                    />
                )}
            </div>
        </div>
    )
}

export default Save;