import "./index.scss";
import { registerBlockType } from '@wordpress/blocks';
import Edit from "./Edit"
import Save from "./Save";
registerBlockType('custom-slider/slider-block', {
	title: 'Custom Slider Block',
	icon: 'format-gallery',
	category: 'design',
	attributes: {
		title: {
			type: 'string'
		},
		paragraph: {
			type: 'string'
		},
		mediaID: {
			type: 'number'
		},
		mediaURL: {
			type: 'string'
		},
		mediaAlt: {
			type: 'string'
		}
	},
	example: {},
	edit: (props) => {
		const {
			className,
			attributes: { title, paragraph, mediaID, mediaURL },
			setAttributes,
		} = props;
		console.log(props)
		const onChangeTitle = (value) => {
			setAttributes({ title: value });
		};

		const onSelectImage = (media) => {
			setAttributes({
				mediaURL: media.url,
				mediaID: media.id,
				mediaAlt: media.alt
			});
		};
		const onChangeParagraph = (value) => {
			setAttributes({ paragraph: value });
		};

		return (
            <Edit 
			className={className}
			title={title} 
			onChangeTitle={onChangeTitle}
			paragraph={paragraph}
			onChangeParagraph={onChangeParagraph} 
			mediaID={mediaID}
			mediaURL={mediaURL}
			onSelectImage={onSelectImage}
			/>
		);
	},
	save: (props) => {
		const {
			className,
			attributes: { title, mediaURL, mediaAlt, paragraph },
		} = props;
		
		return (
			<Save 
			className={className}
			title={title}
			paragraph={paragraph}
			mediaURL={mediaURL}
			mediaAlt={mediaAlt}
			/>
		);
	},
});
